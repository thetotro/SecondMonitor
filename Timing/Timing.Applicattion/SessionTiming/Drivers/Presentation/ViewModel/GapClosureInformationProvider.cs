﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.Presentation.ViewModel
{
    using System;
    using Common.SessionTiming;
    using Common.SessionTiming.Drivers;
    using DataModel.BasicProperties;
    using ViewModels.CarStatus;
    using ViewModels.Settings;
    using ViewModels.Settings.ViewModel;

    public class GapClosureInformationProvider : IGapClosureInformationProvider
    {
        private readonly SessionRemainingCalculator _sessionRemainingCalculator;
        private readonly DisplaySettingsViewModel _displaySettingsViewModel;
        private string _previousEstimation;

        public GapClosureInformationProvider(SessionRemainingCalculator sessionRemainingCalculator, ISettingsProvider settingsProvider)
        {
            _displaySettingsViewModel = settingsProvider.DisplaySettingsViewModel;
            _sessionRemainingCalculator = sessionRemainingCalculator;
            _previousEstimation = string.Empty;
        }

        public string GenerateClosureInformation(DriverTiming driverTiming)
        {
            if (!IsSessionValid(driverTiming))
            {
                return string.Empty;
            }

            DriverTiming player = driverTiming.Session.Player;
            DriverTiming driverInFont;
            DriverTiming driverInBack;

            if (player.Position < driverTiming.Position)
            {
                driverInFont = player;
                driverInBack = driverTiming;
            }
            else
            {
                driverInFont = driverTiming;
                driverInBack = player;
            }

            if (!IsDriverClosing(driverInFont, driverInBack))
            {
                _previousEstimation = string.Empty;
                return _previousEstimation;
            }

            if (_displaySettingsViewModel.GapComputeOnlyOnLapStart && player.CurrentLap?.CurrentlyValidProgressTime.TotalSeconds >= 10)
            {
                return _previousEstimation;
            }

            double gapToClose = Math.Abs(driverTiming.GapToPlayerAbsolute.TotalSeconds) - 1;
            if (gapToClose < 0)
            {
                _previousEstimation = string.Empty;
                return _previousEstimation;
            }

            int positionDifference = driverInBack.Position - driverInFont.Position;
            if (positionDifference > _displaySettingsViewModel.GapClosingMaximumPositionDifference)
            {
                _previousEstimation = string.Empty;
                return _previousEstimation;
            }

            gapToClose += (positionDifference - 1) * _displaySettingsViewModel.GapClosingOvertakeTimeLost;

            double paceDifference = driverInFont.Pace.TotalSeconds - driverInBack.Pace.TotalSeconds;

            int lapsToClose = (int)Math.Ceiling(gapToClose / paceDifference);

            _previousEstimation = lapsToClose - _displaySettingsViewModel.GapClosingExtraLaps > Math.Ceiling(_sessionRemainingCalculator.GetLapsRemaining(driverTiming.Session.LastSet)) ? string.Empty : $"[{lapsToClose}]  ";
            return _previousEstimation;
        }

        private bool IsDriverClosing(DriverTiming driverInFront, DriverTiming driverInBack)
        {
            if (driverInFront.IsLappingPlayer || driverInFront.IsLappedByPlayer || driverInBack.IsLappingPlayer || driverInBack.IsLappedByPlayer)
            {
                return false;
            }

            return driverInFront.Pace > driverInBack.Pace;
        }

        private bool IsSessionValid(DriverTiming driverTiming)
        {
            if (!_displaySettingsViewModel.PaceContainsLapsToCatchEstimation)
            {
                return false;
            }

            ISessionInfo sessionInfo = driverTiming.Session;
            if (driverTiming.IsPlayer || sessionInfo.Player == null || sessionInfo.SessionType != SessionType.Race || sessionInfo.LastSet == null)
            {
                return false;
            }

            if (driverTiming.Pace == TimeSpan.Zero || sessionInfo.Player.Pace == TimeSpan.Zero)
            {
                return false;
            }

            if (sessionInfo.LastSet.SessionInfo.SessionTime.TotalMinutes < _displaySettingsViewModel.GapClosingInitialTime)
            {
                return false;
            }

            return true;
        }
    }
}