﻿namespace SecondMonitor.Timing.Application.SessionTiming.Drivers.Presentation.ViewModel
{
    using Common.SessionTiming.Drivers;

    public interface IGapClosureInformationProvider
    {
        string GenerateClosureInformation(DriverTiming driverTiming);
    }
}