﻿namespace SecondMonitor.Timing.Application.ViewModel
{
    using Contracts.Session;
    using DataModel.Snapshot.Drivers;
    using TimingGrid;

    public class SessionInfoViaTimingViewModelProvider : ISessionInformationProvider
    {
        private TimingDataGridViewModel _timingDataGridViewModel;

        public void RegisterRelay(TimingDataGridViewModel timingDataGridViewModel)
        {
            _timingDataGridViewModel = timingDataGridViewModel;
        }

        public bool IsDriverOnValidLap(IDriverInfo driver)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverOnValidLap(driver);
        }

        public bool IsDriverLastSectorGreen(IDriverInfo driver, int sectorNumber)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverLastSectorGreen(driver, sectorNumber);
        }

        public bool IsDriverLastSectorPurple(IDriverInfo driver, int sectorNumber)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverLastSectorPurple(driver, sectorNumber);
        }

        public bool IsDriverLastClassSectorPurple(IDriverInfo driver, int sectorNumber)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.IsDriverLastClassSectorPurple(driver, sectorNumber);
        }

        public bool HasDriverCompletedPitWindowStop(IDriverInfo driverInfo)
        {
            return _timingDataGridViewModel != null && _timingDataGridViewModel.HasDriverCompletedPitWindowStop(driverInfo);
        }

        public int? GetPlayerPitStopCount()
        {
            return _timingDataGridViewModel != null ? _timingDataGridViewModel.GetPlayerPitStopCount() : null;
        }

        public int GetDriverPitStopCount(IDriverInfo driverInfo)
        {
            return _timingDataGridViewModel != null ? _timingDataGridViewModel.GetDriverPitStopCount(driverInfo) : 0;
        }
    }
}