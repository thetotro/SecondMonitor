﻿using System.Windows.Controls;

namespace ControlTestingApp.Controls
{
    /// <summary>
    /// Interaction logic for PositionCircleTestControl.xaml
    /// </summary>
    public partial class PositionCircleTestControl : UserControl
    {
        public PositionCircleTestControl()
        {
            InitializeComponent();
        }
    }
}
