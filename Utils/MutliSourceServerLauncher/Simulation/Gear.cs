﻿namespace SecondMonitor.Remote.Debug.Simulation
{
    using System;
    internal class Gear
    {
        private double _acceleration;
        private double _offset;

        private double _startTimeElapsed;
        private double _endTimeElapsed;

        public Gear(double acceleration, double offset, double startTimeElapsed, double endTimeElapsed)
        {
            _acceleration = acceleration;
            _offset = offset;
            _startTimeElapsed = startTimeElapsed;
            _endTimeElapsed = endTimeElapsed;
        }

        public static Gear FirstGear(double acceleration, double offset, double endTimeElapsed, double minSpeed)
        {
            return new Gear(acceleration, offset, (minSpeed - offset) / acceleration, endTimeElapsed);
        }

        public static Gear FinalGear(double acceleration, double offset, double startTimeElapsed, double topSpeed)
        {
            return new Gear(acceleration, offset, startTimeElapsed, (topSpeed - offset) / acceleration);
        }

        public double MinSpeed => _offset + _startTimeElapsed * _acceleration;
        public double MaxSpeed => _offset + _endTimeElapsed * _acceleration;

        public double IntegrateSpeed(double currentSpeed, double throttlePosition, double timeStep)
        {
            var previousTimeElapsed = (currentSpeed - _offset) / _acceleration;
            var currentTimeElapsed = previousTimeElapsed + timeStep * throttlePosition;
            return Math.Min(_offset + (_acceleration * currentTimeElapsed), MaxSpeed);
        }
    }
}