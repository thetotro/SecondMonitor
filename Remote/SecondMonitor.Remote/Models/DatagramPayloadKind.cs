﻿namespace SecondMonitor.Remote.Models
{
    public enum DatagramPayloadKind
    {
        Normal,
        SessionStart,
        HearthBeat
    }
}