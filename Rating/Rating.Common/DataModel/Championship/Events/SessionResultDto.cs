﻿namespace SecondMonitor.Rating.Common.DataModel.Championship.Events
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [Serializable]
    public class SessionResultDto
    {
        public SessionResultDto()
        {
            DriverSessionResult = new List<DriverSessionResultDto>();
            ResultCreationTime = DateTime.UtcNow;
            Remarks = string.Empty;
            CompletionPercentage = 1.0;
        }

        public List<DriverSessionResultDto> DriverSessionResult { get; set; }

        [XmlAttribute]
        public DateTime ResultCreationTime { get; set; }

        [XmlAttribute]
        public string Remarks { get; set; }

        /// <summary>
        /// <remarks>0 - 1 Range</remarks>
        /// </summary>
        [XmlAttribute]
        public double CompletionPercentage { get; set; }
    }
}