﻿namespace SecondMonitor.WindowsControls.WPF.PluginsConfig
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for RemoteSettingsControl.xaml
    /// </summary>
    public partial class RemoteConfigurationControl : UserControl
    {
        public RemoteConfigurationControl()
        {
            InitializeComponent();
        }
    }
}
