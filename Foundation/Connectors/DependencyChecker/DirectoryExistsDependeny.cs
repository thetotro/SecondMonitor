﻿namespace SecondMonitor.Foundation.Connectors.DependencyChecker
{
    using System.IO;
    public class DirectoryExistsDependency : IDependency
    {
        public DirectoryExistsDependency(string directoryToCheck)
        {
            this.DirectoryToCheck = directoryToCheck;
        }

        public string DirectoryToCheck { get; }

        public virtual bool ExistsDependency(string basePath)
        {
            return Directory.Exists(Path.Combine(basePath, this.DirectoryToCheck));
        }

        public string GetBatchCommand(string executablePath)
        {
            string dstPath = Path.Combine(executablePath, this.DirectoryToCheck);
            return "md \"" + dstPath + "\"";
        }
    }
}