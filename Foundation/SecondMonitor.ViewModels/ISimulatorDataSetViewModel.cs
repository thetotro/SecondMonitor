﻿namespace SecondMonitor.ViewModels
{
    using DataModel.Snapshot;
    using Properties;

    public interface ISimulatorDataSetViewModel
    {
        void ApplyDateSet(SimulatorDataSet dataSet);

        void Reset();
    }
}