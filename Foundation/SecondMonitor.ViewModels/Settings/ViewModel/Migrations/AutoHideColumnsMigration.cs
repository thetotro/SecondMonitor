﻿namespace SecondMonitor.ViewModels.Settings.ViewModel.Migrations
{
    using System.Linq;
    using DataModel.Extensions;
    using Model;

    public class AutoHideColumnsMigration : ISettingMigration
    {
        private static readonly string[] ColumnsToMigrate = new[]
        {
            "Class Ribbon",
            "Class",
            "Tyre Kind Ribbon",
            "Pit Information",
            "Rating",
            "Points",
        };
        public void MigrateUp(DisplaySettings displaySettings)
        {
            displaySettings.PracticeOptions.ColumnsSettings.Columns.Where(x => ColumnsToMigrate.Contains(x.Name) && x.IsAutoHideCapable == false).ForEach(x =>
            {
                x.IsAutoHideCapable = true;
                x.IsAutoHideEnabled = true;
            });
            displaySettings.QualificationOptions.ColumnsSettings.Columns.Where(x => ColumnsToMigrate.Contains(x.Name) && x.IsAutoHideCapable == false).ForEach(x =>
            {
                x.IsAutoHideCapable = true;
                x.IsAutoHideEnabled = true;
            });
            displaySettings.RaceOptions.ColumnsSettings.Columns.Where(x => ColumnsToMigrate.Contains(x.Name) && x.IsAutoHideCapable == false).ForEach(x =>
            {
                x.IsAutoHideCapable = true;
                x.IsAutoHideEnabled = true;
            });
        }
    }
}