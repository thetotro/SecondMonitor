﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    public enum RefuelingWindowState
    {
        Closed,
        WithContingency,
        NoContingency
    }
}