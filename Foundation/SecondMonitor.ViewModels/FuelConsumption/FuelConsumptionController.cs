﻿namespace SecondMonitor.ViewModels.FuelConsumption
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CarStatus.FuelStatus;
    using Controllers;
    using DataModel.Extensions;
    using DataModel.Snapshot;
    using DataModel.Summary.FuelConsumption;
    using Factory;
    using NLog;
    using SessionEvents;

    public class FuelConsumptionController : AbstractController, IController, IFuelPredictionProvider
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly TimeSpan MinimumSessionLength = TimeSpan.FromMinutes(2);
        private readonly FuelConsumptionRepository _fuelConsumptionRepository;
        private readonly Lazy<OverallFuelConsumptionHistory> _overallFuelHistoryLazy;
        private readonly ISessionEventProvider _sessionEventProvider;
        private readonly IList<ISessionFuelCalculatorConfiguration> _sessionConfigurations;
        private readonly FuelPlannerViewModel _fuelPlannerViewModel;
        private bool _isFuelCalculatorShown;
        private SimulatorDataSet _lastDataSet;
        private ISessionFuelCalculatorConfiguration _activeConfiguration;

        public FuelConsumptionController(FuelConsumptionRepository fuelConsumptionRepository, ISessionEventProvider sessionEventProvider, IList<ISessionFuelCalculatorConfiguration> sessionConfigurations, IViewModelFactory viewModelFactory) : base(viewModelFactory)
        {
            _fuelConsumptionRepository = fuelConsumptionRepository;
            _sessionEventProvider = sessionEventProvider;
            _sessionConfigurations = sessionConfigurations;
            _overallFuelHistoryLazy = new Lazy<OverallFuelConsumptionHistory>(LoadOverallFuelConsumptionHistory);
            _fuelPlannerViewModel = ViewModelFactory.Create<FuelPlannerViewModel>();
            _activeConfiguration = new SessionFuelConfigurationNull();
            Bind<FuelOverviewViewModel>().Command(x => x.ShowFuelCalculatorCommand).To(ShowFuelCalculator);
            Bind<FuelOverviewViewModel>().Command(x => x.HideFuelCalculatorCommand).To(HideFuelCalculatorByUser);
            FuelOverviewViewModel = ViewModelFactory.Create<FuelOverviewViewModel>();
            FuelOverviewViewModel.IsVisible = false;
        }

        public FuelOverviewViewModel FuelOverviewViewModel { get; }
        private OverallFuelConsumptionHistory OverallFuelConsumptionHistory => _overallFuelHistoryLazy.Value;

        private bool IsFuelCalculatorShown
        {
            get => _isFuelCalculatorShown;
            set
            {
                Logger.Info($"FuelCalculator visibility switched to {value}");
                _isFuelCalculatorShown = value;
                //FuelOverviewViewModel.IsVisible = !value;
                FuelOverviewViewModel.FuelPlannerViewModel.IsVisible = value;
            }
        }

        public override Task StartControllerAsync()
        {
            _sessionEventProvider.PlayerPropertiesChanged += SessionEventProviderOnPlayerPropertiesChanged;
            _sessionEventProvider.SessionTypeChange += SessionEventProviderOnSessionTypeChange;
            return Task.CompletedTask;
        }

        public override Task StopControllerAsync()
        {
            _sessionEventProvider.PlayerPropertiesChanged -= SessionEventProviderOnPlayerPropertiesChanged;
            _sessionEventProvider.SessionTypeChange -= SessionEventProviderOnSessionTypeChange;
            StoreCurrentSessionConsumption();
            if (_overallFuelHistoryLazy.IsValueCreated)
            {
                _fuelConsumptionRepository.Save(OverallFuelConsumptionHistory);
            }

            return Task.CompletedTask;
        }

        public void ApplyDataSet(SimulatorDataSet dataSet)
        {
            try
            {
                _lastDataSet = dataSet;
                FuelOverviewViewModel.ApplyDateSet(dataSet);
                if (ShouldOpenFuelCalculator(dataSet))
                {
                    AutoOpenFuelCalculator(dataSet);
                }

                if (ShouldCloseFuelCalculator(dataSet))
                {
                    AutoHideFuelCalculator();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while applying data set");
                throw;
            }
        }

        public void Reset()
        {
            try
            {
                StoreCurrentSessionConsumption();

                if (!string.IsNullOrWhiteSpace(_sessionEventProvider.CurrentCarName))
                {
                    AutoHideFuelCalculator();
                    _sessionConfigurations.ForEach(x => x.Reset());
                    _activeConfiguration = _sessionConfigurations.FirstOrDefault(x => x.SessionType == _sessionEventProvider.LastDataSet.SessionInfo.SessionType) ?? new SessionFuelConfigurationNull();
                }

                if (_overallFuelHistoryLazy.IsValueCreated)
                {
                    _fuelConsumptionRepository.Save(OverallFuelConsumptionHistory);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Error while reseting FuelConsumption Controller");
            }

            FuelOverviewViewModel.Reset();
        }

        private void AutoOpenFuelCalculator(SimulatorDataSet dataSet)
        {
            ShowFuelCalculator();
            (int laps, int minutes, int extraFuel) = _activeConfiguration.GetRequiredRunTime(dataSet, OverallFuelConsumptionHistory);

            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredLaps = laps;
            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.RequiredMinutes = minutes;
            FuelOverviewViewModel.FuelPlannerViewModel.CalculatorForSelectedSession.ExtraFuel = extraFuel;
        }

        private void ShowFuelCalculator()
        {
            try
            {
                if (!Application.Current.Dispatcher.CheckAccess())
                {
                    Application.Current.Dispatcher.Invoke(ShowFuelCalculator);
                    return;
                }

                Logger.Info("Opening Fuel Calculator");

                if (_lastDataSet?.PlayerInfo == null)
                {
                    Logger.Info("Unable to open Fuel Calculator - No Player");
                    return;
                }

                var fuelConsumptionEntries = OverallFuelConsumptionHistory.GetTrackConsumptionHistoryEntries(_lastDataSet.Source, _sessionEventProvider.LastTrackFullName, _sessionEventProvider.CurrentCarName);
                var currentSessionEntry = CreateCurrentSessionFuelConsumptionDto();
                if (currentSessionEntry != null)
                {
                    fuelConsumptionEntries = new[] { currentSessionEntry }.Concat(fuelConsumptionEntries).ToList();
                }

                if (FuelOverviewViewModel.FuelPlannerViewModel != null)
                {
                    FuelOverviewViewModel.FuelPlannerViewModel.Sessions.CollectionChanged -= SessionsOnCollectionChanged;
                }

                _fuelPlannerViewModel.FromModel(fuelConsumptionEntries.ToList());
                FuelOverviewViewModel.FuelPlannerViewModel = _fuelPlannerViewModel;
                IsFuelCalculatorShown = true;

                _fuelPlannerViewModel.Sessions.CollectionChanged += SessionsOnCollectionChanged;
                Logger.Info("Fuel Calculator Opened");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Unable to open Fuel Calculator, Exception");
            }
        }

        private void SessionsOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action != NotifyCollectionChangedAction.Remove)
            {
                return;
            }

            var removedInfo = e.OldItems.OfType<ISessionFuelConsumptionViewModel>().Select(x => x.OriginalModel);
            removedInfo.ForEach(x => OverallFuelConsumptionHistory.RemoveTrackConsumptionHistoryEntry(x));
        }

        private void AutoHideFuelCalculator()
        {
            _activeConfiguration.OnAutoClose();
            IsFuelCalculatorShown = false;
        }

        private void HideFuelCalculatorByUser()
        {
            _activeConfiguration.OnUserClosed();
            IsFuelCalculatorShown = false;
        }

        private void StoreCurrentSessionConsumption()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength ||
                FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel.InLiters < 5)
            {
                Logger.Info("Fuel Consumption - Current Session Discared");
                return;
            }

            OverallFuelConsumptionHistory.AddTrackConsumptionHistoryEntry(CreateCurrentSessionFuelConsumptionDto());
        }

        private SessionFuelConsumptionDto CreateCurrentSessionFuelConsumptionDto()
        {
            if (_lastDataSet?.PlayerInfo == null || FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime < MinimumSessionLength)
            {
                return null;
            }

            var currentSessionFuelConsumption = new SessionFuelConsumptionDto()
            {
                IsWetSession = FuelOverviewViewModel.IsWetSession,
                CarName = _sessionEventProvider.CurrentCarName,
                LapDistance = _lastDataSet.SessionInfo.TrackInfo.LayoutLength.InMeters,
                RecordDate = DateTime.Now,
                SessionKind = _lastDataSet.SessionInfo.SessionType,
                Simulator = _lastDataSet.Source,
                TrackFullName = _lastDataSet.SessionInfo.TrackInfo.TrackFullName,
                TraveledDistanceMeters = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.TraveledDistance.InMeters,
                ConsumedFuel = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ConsumedFuel,
                ElapsedSeconds = FuelOverviewViewModel.FuelConsumptionMonitor.TotalFuelConsumptionInfo.ElapsedTime.TotalSeconds,
            };
            return currentSessionFuelConsumption;
        }

        private void SessionEventProviderOnPlayerPropertiesChanged(object sender, DataSetArgs e)
        {
            Reset();
        }

        private OverallFuelConsumptionHistory LoadOverallFuelConsumptionHistory()
        {
            return _fuelConsumptionRepository.LoadRatingsOrCreateNew();
        }

        public TimeSpan GetRemainingFuelTime()
        {
            return FuelOverviewViewModel.TimeLeft;
        }

        public double GetLapsLeft()
        {
            return FuelOverviewViewModel.LapsLeft;
        }

        private bool ShouldCloseFuelCalculator(SimulatorDataSet dataSet)
        {
            return _activeConfiguration.ShouldAutoClose(dataSet);
        }

        private bool ShouldOpenFuelCalculator(SimulatorDataSet dataSet)
        {
            return _activeConfiguration.ShouldAutoOpen(dataSet);
        }

        private void SessionEventProviderOnSessionTypeChange(object sender, DataSetArgs e)
        {
            Reset();
        }
    }
}