﻿namespace SecondMonitor.ViewModels.TrackRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using DataModel.TrackRecords;
    using Factory;

    public class ClassRecordsViewModel : AbstractViewModel<IEnumerable<NamedRecordSet>>
    {
        private readonly IViewModelFactory _viewModelFactory;
        private ICommand _removeClassCommand;

        public ClassRecordsViewModel(IViewModelFactory viewModelFactory)
        {
            _viewModelFactory = viewModelFactory;
            CarRecordViewModels = new List<CarRecordSummaryViewModel>();
        }

        public TimeSpan OverallBest { get; private set; }

        public string ClassName { get; private set; }

        public string TrackName { get; set; }

        public ICommand RemoveClassCommand
        {
            get => _removeClassCommand;
            set => SetProperty(ref _removeClassCommand, value);
        }

        public List<CarRecordSummaryViewModel> CarRecordViewModels { get; }

        protected override void ApplyModel(IEnumerable<NamedRecordSet> model)
        {
            bool overallInitialized = false;
            foreach (NamedRecordSet vehicleSet in model.OrderBy(x => x.GetOverAllBest().LapTimeSeconds))
            {
                if (!overallInitialized)
                {
                    OverallBest = vehicleSet.GetOverAllBest().LapTime;
                    ClassName = vehicleSet.GetOverAllBest().CarClass;
                    overallInitialized = true;
                }

                CarRecordSummaryViewModel carRecordSummaryViewModel = _viewModelFactory.Create<CarRecordSummaryViewModel>();
                carRecordSummaryViewModel.FromModel(vehicleSet);
                CarRecordViewModels.Add(carRecordSummaryViewModel);
            }
        }

        public override IEnumerable<NamedRecordSet> SaveToNewModel()
        {
            throw new NotSupportedException();
        }
    }
}