﻿namespace SecondMonitor.DataModel.Snapshot
{
    public enum PitWindowState
    {
        None,
        BeforePitWindow,
        InPitWindow,
        AfterPitWindow
    }
}