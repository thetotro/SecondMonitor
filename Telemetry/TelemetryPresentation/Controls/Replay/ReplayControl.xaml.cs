﻿namespace SecondMonitor.TelemetryPresentation.Controls.Replay
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for ReplayControl.xaml
    /// </summary>
    public partial class ReplayControl : UserControl
    {
        public ReplayControl()
        {
            InitializeComponent();
        }
    }
}
